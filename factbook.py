
import os
import re
import sys
import json
import requests
import numpy as np
import pandas as pd

from rich import console
from datetime import date

CIA_ROOT = 'https://www.cia.gov'
FACTBOOK_ROOT = 'https://www.cia.gov/the-world-factbook'


class Log:
    console = console.Console(log_path=False)

    @staticmethod
    def debug(*objects):
        Log.console.print(*objects)

    @staticmethod
    def write(*text, warning=False, error=False):
        if warning:
            kwargs = dict(style='black on yellow')
        elif error:
            kwargs = dict(style='red')
        else:
            kwargs = dict()

        Log.console.log(*text, **kwargs)

    @staticmethod
    def error():
        Log.console.print_exception()
        sys.exit(0)

    @staticmethod
    def divider():
        Log.console.rule()


def xpath(query: str) -> tuple[str, str]:
    return ('xpath', query)


def snakecase(text: str):
    text = re.sub(r'[^a-z0-9_\-\\\/ ]', r'', text.lower())

    while '  ' in text:
        text = text.replace('  ', ' ')

    text = re.sub(r'[ \-\\\/]', '_', text.strip())

    while '__' in text:
        text = text.replace('__', '_')

    text = re.sub(r'_$', r'', text)
    text = re.sub(r'^_', r'', text)

    return text


def convert_empty_string_to_none(n: str):
    return None if len(n) == 0 else n


def convert_empty_string_to_nan(n: str):
    return np.nan if n is None or len(n) == 0 else n


def filter_blank_records(n: list):
    return [i for i in n if len(i.strip()) > 0]


def extract_group(group_number: int):
    def extract_func(n):
        return n.group(group_number) if isinstance(n, re.Match) else None

    return extract_func


def create_country_key(country: str) -> str:
    country = country.lower()
    country = country.replace(' ', '-')
    country = re.sub(r'[^a-z\-]', '', country)
    return country


def extract_country_key(factbook_url: str) -> str:
    return os.path.normpath(factbook_url).split('/')[-1]


def create_country_page_data_url(country_key: str) -> str:
    return f'{FACTBOOK_ROOT}/page-data/countries/{country_key}/page-data.json'


def clean_factbook_directory_record(record: dict) -> dict:
    country = record['title']
    country_key = create_country_key(country)
    page_data_url = create_country_page_data_url(country_key)

    redirect_url = record['redirect']
    if redirect_url != '':
        redirect_url = FACTBOOK_ROOT + redirect_url
    else:
        redirect_url = None

    summary_document = record.get('summaryDocument')
    if summary_document is not None:
        summary_document = summary_document.get('localFile')
        summary_document = summary_document.get('publicURL')
        summary_pdf_url = CIA_ROOT + summary_document
    else:
        summary_pdf_url = None

    return dict(country_name=country, country_key=country_key,
                page_data_url=page_data_url, redirect_url=redirect_url,
                summary_pdf_url=summary_pdf_url)


def download_factbook_directory():
    factbook_directory = f'{FACTBOOK_ROOT}/page-data/countries/page-data.json'
    response = requests.get(factbook_directory)
    response_json = response.json()

    try:
        results = response_json['result']['data']['countries']['edges']
        results = list(map(lambda n: n['node'], results))
    except KeyError:
        Log.error()

    results = list(map(clean_factbook_directory_record, results))
    return results


def process_factbook_country(country_key: str, page_data_url: str, redirect_url: str,
                             summary_pdf_url: str) -> dict:

    Log.write(country_key)
    if redirect_url is not None:
        redirect_country_key = extract_country_key(redirect_url)
        data_url = create_country_page_data_url(redirect_country_key)
    else:
        data_url = page_data_url

    # if summary_pdf_url is not None:
    #     response = requests.get(summary_pdf_url)
    #     if response.status_code != 200:
    #         raise Exception(f'{response.status_code} error: {summary_pdf_url}')

    #     summary_pdf = response.content
    # else:
    #     summary_pdf = None

    response = requests.get(data_url)
    if response.status_code != 200:
        raise Exception(f'{country_key}: {response.status_code} error @ {data_url}')

    response_json = response.json()

    travel = response_json['result']['pageContext']['travel']
    if travel is None:
        country_record = dict(country_key=country_key)
    else:
        country_record = dict(country_key=country_key, **travel)

    country_json = response_json['result']['data']['country']['json']
    country_json = json.loads(country_json)
    country_record = dict(country_record, **country_json)
    # country_record['summary_pdf'] = summary_pdf

    try:
        del country_json['footer']
    except KeyError:
        pass

    return country_record


def download_factbook():
    # driver, wait = new_factbook_session()
    factbook_directory = download_factbook_directory()

    country_records = list()

    for record in factbook_directory:
        record = (record['country_key'], record['page_data_url'],
                  record['redirect_url'], record['summary_pdf_url'])

        country = process_factbook_country(*record)
        country_records.append(country)

    country_records = map(lambda n: {snakecase(k): v for k, v in n.items()}, country_records)
    factbook = dict(countries=list(country_records))
    with open('factbook.json', 'w') as factbook_json_file:
        json.dump(factbook, factbook_json_file)


def process_factbook():
    factbook = pd.read_json('factbook.json')
    countries = pd.DataFrame(list(factbook['countries']))
    countries = countries.set_index('country_key')

    media = pd.DataFrame(countries['media'].explode())
    media = pd.DataFrame(list(media['media']), index=media.index)

    categories = pd.DataFrame(countries['categories'].explode())
    categories = pd.DataFrame(list(categories['categories']), index=categories.index)

    fields = categories[['title', 'fields']].explode('fields')
    fields = fields.reset_index().set_index(['country_key', 'title'])
    fields = pd.DataFrame(list(fields['fields']), index=fields.index)
    fields = fields.rename(columns={'title': 'field'})
    fields = fields[['field', 'content']]
    fields = fields.pivot(columns='field', values='content')
    fields = fields.reset_index()

    html_tag = re.compile(r'</?[a-z]+>')
    linebreaks = re.compile(r'<br ?\/?>')

    for category_name in fields['title'].unique():
        category = pd.DataFrame(fields[fields['title'] == category_name])
        category = category.dropna(axis=1, how='all')
        category.columns = map(snakecase, category.columns)
        category = category.drop(columns='title')
        category = category.set_index('country_key')
        category = category.fillna('')
        category = category.applymap(lambda n: linebreaks.sub('\n', n))
        category = category.applymap(lambda n: html_tag.sub('', n))
        category = category.applymap(lambda n: n.replace('&nbsp;', ''))

        category.to_parquet(f'factbook/{snakecase(category_name)}.snappy.parquet')

    countries = countries.drop(columns=['categories', 'media'])
    countries.to_parquet('factbook/factbook.snappy.parquet')


def analyze_introductions():
    introductions = pd.read_parquet('factbook_introduction.snappy.parquet')
    introductions = introductions.drop(columns='preliminary_statement')
    Log.debug(introductions)


def analyze(dataset_name: str):
    df = pd.read_parquet(f'factbook/{dataset_name}.snappy.parquet')

    chief_of_state_pattern = re.compile(r'^chief of state: (.+)$')

    executive_branch = df['executive_branch']
    executive_branch = executive_branch.apply(lambda n: n.split('\n')[0])

    executive_branch = executive_branch.apply(chief_of_state_pattern.search)
    executive_branch = executive_branch.apply(extract_group(1))

    Log.debug(executive_branch)


if __name__ == '__main__':
    # download_factbook()
    process_factbook()
    analyze('government')
